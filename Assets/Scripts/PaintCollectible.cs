﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class PaintCollectible : MonoBehaviour
{
    public GameObject paintSpray;
    public float addedQuantity = 1500f;

    private RayCastPainter m_Painter;
    // Start is called before the first frame update
    void Start()
    {
        this.m_Painter = this.paintSpray.GetComponent<RayCastPainter>();
    }

    private void OnTriggerEnter(Collider other)
    {
        try
        {
            if (other.CompareTag("Player"))
            {
                this.m_Painter.AddPaint(this.addedQuantity);
                Debug.Log($"Ajout de {this.addedQuantity}L de peinture.");
//                Object.Destroy(this.gameObject);
            }
        }
        catch (Exception e)
        {
            Debug.Log("Cannot colect paint : " + e.Message);
        }
    }
}
