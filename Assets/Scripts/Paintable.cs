﻿#region USING

using UnityEngine;

#endregion

public class Paintable : MonoBehaviour
{
	#region CONSTANTES ET ATTRIBUTS STATIQUES

	private const int textureWidth = 128;
	private const int textureHeight = 128;
	private const string shaderName = "Custom/TestShader";
	private static readonly int drawingTex = Shader.PropertyToID("_DrawingTex");

	#endregion

	#region ATTRIBUTS

	private readonly Color m_Color = new Color(0, 0, 0, 0);

	private Material m_Material;
	private Texture2D m_Texture;

	#endregion

	#region MÉTHODES

	private void Start()
	{
		Renderer rend = GetComponent<Renderer>();
		if (rend != null)
		{
			foreach (Material material in rend.materials)
			{
				if (material.shader.name == Paintable.shaderName)
				{
					this.m_Material = material;
					break;
				}
			}

			if (this.m_Material != null)
			{
				Vector3 localScale = this.transform.localScale;
				this.m_Texture = new Texture2D((int) (Paintable.textureWidth * localScale.x), (int) (Paintable.textureHeight * localScale.y));
				for (int x = 0; x < this.m_Texture.width; x++)
				{
					for (int y = 0; y < this.m_Texture.height; y++)
					{
						this.m_Texture.SetPixel(x, y, this.m_Color);
					}
				}

				this.m_Texture.Apply();

				this.m_Material.SetTexture(Paintable.drawingTex, this.m_Texture);
			}
		}
	}

	public void Paint(Vector2 textureCoord, Texture2D brushTexture, Color paintColor, float brushSize)
	{
		int newWidth = (int) (brushTexture.width * brushSize);
		int newHeight = (int) (brushTexture.height * brushSize);
		Rect texR = new Rect(0, 0, newWidth, newHeight);
		
		brushTexture.Apply(true);
		RenderTexture rtt = new RenderTexture(newWidth, newHeight, 32);
		Graphics.SetRenderTarget(rtt);
		GL.LoadPixelMatrix(0, 1, 1, 0);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.DrawTexture(new Rect(0, 0, 1, 1), brushTexture);

		Texture2D resTex = new Texture2D(newWidth, newHeight, TextureFormat.ARGB32, true);
		resTex.Resize(newWidth, newHeight);
		resTex.ReadPixels(texR, 0, 0, true);
		int x = (int) (textureCoord.x * this.m_Texture.width) - resTex.width / 2;
		int y = (int) (textureCoord.y * this.m_Texture.height) - resTex.height / 2;
		for (int i = 0; i < resTex.width; i++)
		for (int j = 0; j < resTex.height; j++)
		{
			int finX = x + i;
			int finY = y + j;
			Color existingColor = this.m_Texture.GetPixel(finX, finY);
			Color baseBrushColor = resTex.GetPixel(i, j);
			float alpha = baseBrushColor.a;
			if (alpha > 0)
			{
				paintColor.a = alpha;
				Color result = Color.Lerp(existingColor, paintColor, alpha);
				result.a = existingColor.a + alpha;
				this.m_Texture.SetPixel(finX, finY, result);
			}
		}

		this.m_Texture.Apply();
	}

	private static void _gpu_scale(Texture2D src, int width, int height, FilterMode fmode)
	{
	}

	#endregion
}