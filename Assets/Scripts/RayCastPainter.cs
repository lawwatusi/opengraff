﻿#region USING

using System;
using UnityEngine;
using UnityEngine.UI;

#endregion

public class RayCastPainter : MonoBehaviour
{
    #region ATTRIBUTS

    private const float minSize = .5f;
    private const float maxSize = 4f;
    public float sizeModifier = 1;
    public float sizeIncrease = 1.5f;
    public float range = 3.5f;

    public Color sprayColor;
    public float paintUsed = .2f;
    private float m_MaxAvailablePaint = 1500;
    private float m_AvailablePaint;
    public GameObject paintBar;
    private Slider m_Slider;
    public Texture2D brushTexture;
//  public Camera cam;
    public GameObject origin;

    #endregion

    #region MÉTHODES

    private void Start()
    {
        this.m_Slider = this.paintBar.GetComponent<Slider>();

        this.m_AvailablePaint = this.m_Slider.maxValue = this.m_MaxAvailablePaint;
        this.m_Slider.value = this.m_AvailablePaint;
    }

    // Update is called once per frame
    private void Update()
    {
        this.m_Slider.value = this.m_AvailablePaint;
        if (Input.GetButton("Fire1") && this.m_AvailablePaint > 0)
        {
            if (Physics.Raycast(this.origin.transform.position, this.origin.transform.forward, out RaycastHit hit, this.range))
            {
                Paintable paintable = hit.transform.GetComponent<Paintable>();
                if (paintable != null)
                {
                    paintable.Paint(hit.textureCoord, this.brushTexture, this.sprayColor, this.sizeModifier);
                    RemovePaint(this.paintUsed * this.sizeModifier);
                }
            }
        }

        if (Math.Abs(Input.mouseScrollDelta.y) > 0)
        {
            this.sizeModifier += Mathf.CeilToInt(Input.mouseScrollDelta.y) * this.sizeIncrease * Time.deltaTime;
            this.sizeModifier = Mathf.Clamp(this.sizeModifier, RayCastPainter.minSize, RayCastPainter.maxSize);
        }
    }

    public void RemovePaint(float quantity)
    {
        if (this.m_AvailablePaint - quantity >= 0)
            this.m_AvailablePaint -= quantity;
        else
            this.m_AvailablePaint = 0;
    }

    public void AddPaint(float quantity)
    {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if(this.m_AvailablePaint == this.m_MaxAvailablePaint)
            throw new InvalidOperationException("The paint spray is already full...");
        if (this.m_AvailablePaint + quantity <= this.m_MaxAvailablePaint)
            this.m_AvailablePaint += quantity;
        else
            this.m_AvailablePaint = this.m_MaxAvailablePaint;
    }
    #endregion
}